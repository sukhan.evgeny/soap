package sukhan.mycompany.web;

import sukhan.mycompany.App;

public class HelloSingleton {
    private static volatile HelloSingleton instance;

    public static HelloSingleton getInstance() {
        HelloSingleton localInstance = instance;
        if (localInstance == null) {
            synchronized (HelloSingleton.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new HelloSingleton();
                    //Say Hello
                    final App app = new App();
                    app.displayWelcome();
                }
            }
        }
        return localInstance;
    }
}