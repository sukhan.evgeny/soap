package sukhan.mycompany.web;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ServerMessage {
    @WebMethod
    String sendMessage(@WebParam(name = "message") String message);
}
