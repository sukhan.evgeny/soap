package sukhan.mycompany.web;

import sukhan.mycompany.App;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import java.util.Scanner;

import static sukhan.mycompany.constant.TerminalConst.EXIT;

@WebService(endpointInterface = "sukhan.mycompany.web.ServerMessage",
        serviceName = "ServerMessage")
public class MessageSender implements ServerMessage {

    @WebMethod
    public String sendMessage(@WebParam(name = "message") String message) {
        HelloSingleton.getInstance();
        final Scanner scanner = new Scanner(System.in);
        final App app = new App();
        app.displayWelcome();
        app.run(message);
        return "potencial logs";
    }
}
