package sukhan.mycompany;

import org.junit.Assert;
import org.junit.Test;

public class AppTest {
    private final App app = new App();
    @Test
    public void appTestForWorking() {
        Assert.assertNotNull(app.run("version"));
    }
    @Test(timeout=50)
    public void appTestForNoError() {
        Assert.assertEquals(app.run("version"), 0);
    }
    @Test
    public void appTestForError() {
        Assert.assertEquals(app.run("error"), -1);
    }
}
