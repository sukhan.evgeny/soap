package sukhan.mycompany;

import org.junit.Assert;
import org.junit.Test;
import sukhan.mycompany.controller.ProjectController;
import sukhan.mycompany.repository.ProjectRepository;
import sukhan.mycompany.repository.TaskRepository;
import sukhan.mycompany.service.ProjectService;

public class ProjectControllerTest {
    private final ProjectRepository projectRepository = new ProjectRepository();
    private final ProjectService projectService = new ProjectService(projectRepository);
    private final ProjectController projectController = new ProjectController(projectService);

    @Test
    public void projectServiceCreateTest() {
        Assert.assertEquals(projectController.createProject(), 0);
    }

}
