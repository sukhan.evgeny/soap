package sukhan.mycompany;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import sukhan.mycompany.entity.Project;
import sukhan.mycompany.repository.ProjectRepository;
import sukhan.mycompany.repository.TaskRepository;
import sukhan.mycompany.service.ProjectService;

public class ProjectServiceTest {

    private final ProjectRepository projectRepository = new ProjectRepository();
    private final ProjectService projectService = new ProjectService(projectRepository);
    private Long id;
    private final String projectName = "projectTest";
    private final String projectName2 = "projectSuperTest";

    @Before
    public void projectServiceCreateBeforeTest() {
        this.id = projectService.create(projectName).getId();
    }

    @Test(timeout=100)
    public void projectServiceCreateTest() {
        Assert.assertNotNull(projectService.findByName(projectName));
        Assert.assertEquals(projectService.findByName(projectName).getId(), id);
    }

    @Test(timeout=100)
    public void projectServiceUpdateTest() {
        Assert.assertNotNull(projectService.update(id, projectName2, "TEST"));
        Assert.assertNotNull(projectService.findByName(projectName2));
    }

    @Test
    public void projectServiceDeleteTest() {
        Assert.assertNotNull(projectService.removeById(id));
        Assert.assertNull(projectService.findByName(projectName));
    }
}
