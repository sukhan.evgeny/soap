package sukhan.mycompany;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import sukhan.mycompany.repository.TaskRepository;

public class TaskRepositoryTest {
    private final TaskRepository taskRepository = new TaskRepository();
    private Long id;
    private final String projectName = "projectTest";

    @Before
    public void taskRepositoryCreateBeforeTest() {
        this.id = taskRepository.create(projectName).getId();
    }

    @Test
    public void taskRepositoryCreateTest() {
        Assert.assertNotNull(taskRepository.findByName(projectName));
        Assert.assertEquals(taskRepository.findByName(projectName).getId(), id);
    }

    @Test(expected = NullPointerException.class)
    public void taskRepositoryRemoveByIdTestWithNull() {
        Assert.assertNotNull(taskRepository.removeById(id));
        Assert.assertNull(taskRepository.findByName(projectName).getId());
    }
    @Test
    public void taskRepositoryRemoveByIdTest() {
        Assert.assertNotNull(taskRepository.removeById(id));
        Assert.assertNull(taskRepository.findByName(projectName));
        Assert.assertNull(taskRepository.findById(id));
    }
    @Test
    public void taskRepositoryRemoveByNameTest() {
        Assert.assertNotNull(taskRepository.removeByName(projectName));
        Assert.assertNull(taskRepository.findByName(projectName));
        Assert.assertNull(taskRepository.findById(id));
    }
}
